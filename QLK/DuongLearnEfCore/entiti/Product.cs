﻿using DuongLearnEfCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuongLearnEfCore.entiti
{
    [Table("Products")]
    public class Product
    {
        [Key]
        public Guid ProductId { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string ProductName { get; set; }

        [Column(TypeName = "decimal(18,0")]
        [Required]
        public decimal Price { get; set; }

        [Column(TypeName = "datetime")]
        [Required]
        public DateTime ManufactoringDate { get; set; }
        [Column(TypeName = "datetime")]
        [Required]
        public DateTime CreatedDate { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string Description { get; set; }
        [Required]
        public EntityStatus Status { get; set; }
        [Required]

        public Guid CategoryId { get; set; }
        [Required]

        public Guid UserId { get; set; }
        [Required]
        [Column(TypeName = "int")]

        public int Quantity { get; set; }
    }
}


