﻿using DuongLearnEfCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuongLearnEfCore.entiti
{
    [Table("Categoryies")]
    public class Category
    {
        [Required]
        [ForeignKey("Product-Category")]
        public Guid CategoryId {  get; set; }

        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string CategoryName{ get; set; }

        [Required]
        public DateTime CategoryDate { get; set; }

        [Required]
        public EntityStatus Status { get; set; }
        
        public IList<Product> Products { get; set; }

    }
}
